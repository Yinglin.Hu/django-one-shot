from django.shortcuts import render
from todos.models import TodoList

# Create your views here.
def todo_list_list(request):
    lists = TodoList.objects.all()
    context = {
        "lists": lists,
    }
    return render(request, "todos/my_list.html", context)
